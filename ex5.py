name = 'Jon Stump'
age = 37
height = 73
weight = 180
eyes = 'brown'
teeth = 'white'
hair = 'brown'


print(f"Let's talk about {name}.")
print(f"He's {height} inches tall.")
print(f"He's {weight} pounds heavy.")
print("Actuall that's not too heavy.")
print(f"He's got {eyes} eyes and {hair} hair.")
print(f"His teeth are usually {teeth} depending on the coffee.")

# This line is tricky, try to get it exactly right
total = age + height + weight
print(f"If I add {age}, {height}, and {weight} I get {total}.")

print(f"If I convert my weight and height to metric, my weight is {weight * 0.45} kg and my height is {height * 2.54} centimeters.")
